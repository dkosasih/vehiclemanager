﻿using System;
using System.Collections.Generic;
using System.Text;
using VehicleManager.Domain.Entity.CarEntity;

namespace VehicleManager.Persistence.Test.Helper
{
    public static class VehicleDbContextSeederExtension
    {
        public static void EnsureDbSeeded(this VehicleDbContext context)
        {
            // ensure data is clean 
            context.Vehicles.RemoveRange(context.Vehicles);

            context.SaveChanges();

            var honda = new Car { Id = "car001", Make = "Honda", BodyType = "Sedan", VehicleType = 1, Door = 4, Engine = "VTech", Model = "Accord", Wheel = 4 };

            context.Vehicles.Add(honda);
            context.SaveChanges();
        }
    }
}
