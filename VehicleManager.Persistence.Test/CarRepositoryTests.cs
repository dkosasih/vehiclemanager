using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Linq;
using System.Threading.Tasks;
using VehicleManager.Domain.Entity.CarEntity;
using VehicleManager.Persistence.Repository;
using VehicleManager.Persistence.Test.Helper;

namespace VehicleManager.Persistence.Test
{
    [TestClass]
    public class CarRepositoryTest
    {
        private ICarRepository _carRepository;
        private VehicleDbContext _vehicleDbContext;

        [TestInitialize]
        public void Init()
        {
            var options = new DbContextOptionsBuilder<VehicleDbContext>()
                .UseInMemoryDatabase(databaseName: "Vehicle_Context_Mock")
                .Options;

            _vehicleDbContext = new VehicleDbContext(options);
            _vehicleDbContext.Database.EnsureCreated();
            _vehicleDbContext.EnsureDbSeeded();
            _carRepository = new CarRepository(_vehicleDbContext);
        }

        [TestCleanup]
        public void Cleanup()
        {
            _vehicleDbContext.Dispose();
        }

        [TestMethod]
        public async Task GetAll_Retrieve_All()
        {
            // Act
            var cars = await _carRepository.GetAll();

            //Assert
            Assert.AreEqual(cars.Count, 1);
        }

        [TestMethod]
        public async Task GivenId_GetById_SpecificCar()
        {
            // Arrange
            var searchId = "car001";

            // Act
            var car = await _carRepository.GetById(searchId);

            //Assert
            Assert.AreEqual(car.Make, "Honda");
            Assert.AreEqual(car.BodyType, "Sedan");
            Assert.AreEqual(car.Wheel, 4);
        }

        [TestMethod]
        public async Task GivenNonExistenceId_GetById_Null()
        {
            // Arrange
            var searchId = "not-exists-id";

            // Act
            var car = await _carRepository.GetById(searchId);

            //Assert
            Assert.AreEqual(car, null);
        }

        [TestMethod]
        public async Task GivenId_DeleteById_RemoveItem()
        {
            // Arrange
            var searchId = "car001";

            // Act
            await _carRepository.Delete(searchId);
            var car = await _vehicleDbContext.Vehicles.FindAsync(searchId);

            //Assert
            Assert.AreEqual(car, null);
        }

        [TestMethod]
        public async Task GivenProduct_AddCar_NewCarAdded()
        {
            // Arrange
            var carToInsert = new Car
            {
                Make = "Audi",
                BodyType = "Hatchback",
                Door = 3,
                VehicleType = 1,
                Model = "R8"
            };

            // Act
            await _carRepository.Add(carToInsert);
            var car = await _vehicleDbContext.Vehicles.OfType<Car>().FirstOrDefaultAsync(c => c.Id == carToInsert.Id);

            //Assert
            Assert.AreEqual(car.Id, carToInsert.Id);
            Assert.AreEqual(car.Make, carToInsert.Make);
            Assert.AreEqual(car.Door, carToInsert.Door);
            Assert.AreEqual(car.VehicleType, carToInsert.VehicleType);
        }
        
        [TestMethod]
        public async Task GivenProductWithSameId_AddCar_ExceptionThrown()
        {
            // Arrange
            var existingCar = new Car { Id = "car001", Make = "Honda", BodyType = "Sedan", VehicleType = 1, Door = 4, Engine = "VTech", Model = "Accord", Wheel = 4 };

            // Act
            await Assert.ThrowsExceptionAsync<InvalidOperationException>(() => _carRepository.Add(existingCar));
        }

        [TestMethod]
        public async Task GivenProductAndId_UpdateCar_CarUpdated()
        {
            // Arrange
            var carToUpdate = new Car
            {
                Id = "car001",
                Make = "Audi",
                BodyType = "Hatchback"
            };


            // Act
            await _carRepository.Update(carToUpdate.Id, carToUpdate);
            var car = await _vehicleDbContext.Vehicles.OfType<Car>().FirstOrDefaultAsync(c => c.Id == carToUpdate.Id);

            //Assert
            Assert.AreEqual(car.Id, carToUpdate.Id);
            Assert.AreEqual(car.Make, carToUpdate.Make);
            Assert.AreEqual(car.BodyType, carToUpdate.BodyType);
        }
    }
}
