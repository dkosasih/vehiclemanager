`Dotnet Core` is chosen to use the **latest** technology. However, `EF Core` has yet to support the TPT database inheritance strategy.
I choose to go with TPH with discriminator in order to be able to prepare for future migration once TPT is supported.

I had an option to go with `composition` pattern  implement `TPT` inheritence database mapping. However, I would love to use `poymorphism`, that is why I have choosen to use TPH.

Front-end is chosen to be using Angular, NgRX for state management, and Jest for testing.


**Running the application**

To run the API on CLI: 

- `dotnet run` in Vehicle.Manager.Api folder.

- Or run via Visual Studio start button. It should attach the server via IISExpress and open `swagger` url.

- It will run on port 36388 by default. 



To run the front-end Angular application:

- `npm install`

- `npm start -- --project=vehicle-manager`

- It should have been configured to call to http://localhost:36288, if not, the configuration is in environment folder.


**Running the tests**

To run all the backend unit tests:

- go to project root folder

- `dotnet test`

To run front-end unit tests:

- go to vehicle-manager-ui folder

- `npm test`