﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VehicleManager.Domain.Base
{
    public interface IEntityRoot
    {
        string Id { get; set; }
    }
}
