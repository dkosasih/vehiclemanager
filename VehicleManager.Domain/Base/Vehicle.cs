﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VehicleManager.Domain.Base
{
    public abstract class Vehicle : IEntityRoot
    {
        public string Id { get; set; }
        // TODO: Change this to use picklist
        public int VehicleType { get; set; }
        public string Make { get; set; }
        public string Model { get; set; }
        public string Engine { get; set; }
    }
}
