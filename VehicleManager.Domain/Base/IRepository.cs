﻿using System.Threading.Tasks;

namespace VehicleManager.Domain.Base
{
    public interface IRepository<T>
    {
        Task<T> GetById(string id);
    }
}
