﻿using VehicleManager.Domain.Base;

namespace VehicleManager.Domain.Entity.CarEntity
{
    public class Car : Vehicle
    {
        public int Wheel { get; set; }
        public int Door { get; set; }
        public string BodyType { get; set; }
    }
}
