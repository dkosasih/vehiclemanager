﻿using System.Collections.Generic;
using System.Threading.Tasks;
using VehicleManager.Domain.Base;

namespace VehicleManager.Domain.Entity.CarEntity
{
    public interface ICarRepository : IRepository<Car>
    {
        Task<IList<Car>> GetAll();
        Task<string> Add(Car car);
        Task Delete(string id);
        Task Update(string id, Car car);
    }
}
