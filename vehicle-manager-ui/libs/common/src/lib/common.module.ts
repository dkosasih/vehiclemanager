import { NgModule, InjectionToken, ModuleWithProviders } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TextMaskModule } from 'angular2-text-mask';
import { MatSnackBarModule } from '@angular/material';

export const API_HOST: InjectionToken<string> = new InjectionToken<string>('api-host');

@NgModule({
  imports: [
    CommonModule,
    MatSnackBarModule
  ],
  exports: [
    TextMaskModule
  ]
})
export class VehicleManagerCommonModule {
  public static forRoot(hostUrl: string): ModuleWithProviders {
    return {
      ngModule: VehicleManagerCommonModule,
      providers: [
        {provide: API_HOST, useValue: hostUrl}
      ]
    }
  }

  public static forFeature(): ModuleWithProviders {
    return {
      ngModule: VehicleManagerCommonModule
    }
  }

}
