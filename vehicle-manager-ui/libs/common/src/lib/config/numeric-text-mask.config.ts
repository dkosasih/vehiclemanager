import createNumberMask from 'text-mask-addons/dist/createNumberMask';

export function numberOnlyMask(maxDigit: number) {
    return createNumberMask({
        prefix: '',
        includeThousandsSeparator: false,
        integerLimit: maxDigit
    });
}
