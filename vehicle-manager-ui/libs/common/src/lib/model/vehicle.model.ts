export interface Vehicle {
  id: string;
  vehicleType: number;
  make: string;
  model: string;
  engine: string;
}
