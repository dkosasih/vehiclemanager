export { VehicleManagerCommonModule } from './lib/common.module';

export * from './lib/model/vehicle.model';
export * from './lib/config/numeric-text-mask.config';

export * from './lib/service/base-data.service';
export * from './lib/service/snack-bar.service';
