module.exports = {
  name: 'vehicle-manager',
  preset: '../../jest.config.js',
  coverageDirectory: '../../coverage/apps/vehicle-manager',
  snapshotSerializers: [
    'jest-preset-angular/AngularSnapshotSerializer.js',
    'jest-preset-angular/HTMLCommentSerializer.js'
  ]
};
