import { Vehicle } from '@vehicle-manager-ui/common';

export interface Car extends Vehicle {
   wheel: number;
   door: number;
   bodyType: string;
}
