import { Injectable, Inject, Injector } from '@angular/core';
import { BaseDataService, SnackBarService } from '@vehicle-manager-ui/common';
import { HttpClient } from '@angular/common/http';
import { Car } from '../model/car.model';

@Injectable({ providedIn: 'root' })
export class CarService extends BaseDataService {
  private readonly baseRouteUrl = '/api/car';

  constructor(
    httpService: HttpClient,
    injector: Injector,
    snackBarService: SnackBarService
  ) {
    super(httpService, injector, snackBarService);
  }

  getCars() {
    return this.baseHttpGet<null, Car[]>(this.baseRouteUrl);
  }

  addCar(car: Car) {
    // TODO make this a stongly typed by introducing return type of commandResult
    return this.baseHttpPost<Car, any>(this.baseRouteUrl, car);
  }

  deleteCar(id: string) {
    return this.baseHttpDelete(`${this.baseRouteUrl}/${id}`);
  }

  updateCar(car: Car) {
    // TODO make this a stongly typed by introducing return type of commandResult
    return this.baseHttpPut(`${this.baseRouteUrl}/${car.id}`, car);
  }
}
