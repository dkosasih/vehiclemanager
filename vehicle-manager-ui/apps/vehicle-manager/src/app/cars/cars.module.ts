import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { CarsContainerComponent } from './cars-container.component';
import { CarFormComponent } from './component/car-form/car-form.component';
import { AngularMaterialModule } from '../material.module';
import { ReactiveFormsModule } from '@angular/forms';
import { VehicleManagerCommonModule } from '@vehicle-manager-ui/common';
import { CarListComponent } from './component/car-list/car-list.component';

const routes: Routes = [
  { path: '', component: CarsContainerComponent },
  { path: 'new', component: CarFormComponent }
];

@NgModule({
  declarations: [CarsContainerComponent, CarFormComponent, CarListComponent],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    AngularMaterialModule,
    VehicleManagerCommonModule.forFeature(),
    RouterModule.forChild(routes)
  ]
})
export class CarsModule { }
