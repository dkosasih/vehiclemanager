import { async, ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';

import { CarFormComponent } from './car-form.component';
import { CommonModule, Location } from '@angular/common';
import { AngularMaterialModule } from '../../../material.module';
import { ReactiveFormsModule } from '@angular/forms';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { Actions } from '@ngrx/effects';
import { SnackBarService, VehicleManagerCommonModule } from '@vehicle-manager-ui/common';
import { By } from '@angular/platform-browser';
import { provideMockActions } from '@ngrx/effects/testing';
import { createSpyObj } from '../../../../test-helper/createSpyObj';
import { addSuccess, addFailed } from '../../../store/action/car.action';
import { of, Observable } from 'rxjs';


describe('CarFormComponent', () => {
  let actionMock$: Observable<any>;

  let component: CarFormComponent;
  let fixture: ComponentFixture<CarFormComponent>;

  beforeEach(async(() => {
    const locationSpy = createSpyObj('location', ['back']);
    const storeSpy = createSpyObj('store', ['dispatch']);
    const routerSpy = createSpyObj('router', ['navigate']);
    const snackbarServiceSpy = createSpyObj('snackbar', ['success', 'error']);

    TestBed.configureTestingModule({
      imports: [
        CommonModule,
        AngularMaterialModule,
        ReactiveFormsModule,
        NoopAnimationsModule,
        VehicleManagerCommonModule.forFeature(),
      ],
      providers: [
        { provide: Location, useValue: locationSpy },
        { provide: Router, useValue: routerSpy },
        { provide: Store, useValue: storeSpy },
        provideMockActions(() => actionMock$),
        { provide: SnackBarService, useValue: snackbarServiceSpy }
      ],
      declarations: [
        CarFormComponent,
      ],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CarFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  test('should create the component', () => {
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  });

  test('should disable submit button when form appear', () => {
    const submitButton = fixture.debugElement.query(By.css('button[data-testId="btnSubmit"]'));

    fixture.detectChanges();

    expect(submitButton.nativeElement.getAttribute('disabled')).toEqual("true");
  });

  test('should enable submit button when form appear', () => {
    const submitButton = fixture.debugElement.query(By.css('button[data-testId="btnSubmit"]'));

    fixture.detectChanges();

    fillForm(fixture);

    fixture.detectChanges();

    expect(submitButton.nativeElement.getAttribute('disabled')).toEqual(null);
  });

  test('should called save and store dispatch action', () => {
    const actionPipe = TestBed.get(Actions);
    const store = TestBed.get(Store);
    actionPipe.pipe = jest.fn(() => of(addSuccess('abcd')));
    const submitButton = fixture.debugElement.query(By.css('button[data-testId="btnSubmit"]'));

    fixture.detectChanges();

    fillForm(fixture);

    fixture.detectChanges();

    expect(submitButton.nativeElement.getAttribute('disabled')).toEqual(null);
    submitButton.nativeElement.click();

    fixture.detectChanges();

    expect(store.dispatch).toBeCalledTimes(1);
  });

  test('should call snackbar.error and navigate on success',() => {
    const store = TestBed.get(Store);
    const rter = TestBed.get(Router);
    const sbs = TestBed.get(SnackBarService);
    const submitButton = fixture.debugElement.query(By.css('button[data-testId="btnSubmit"]'));
    actionMock$ = of(addSuccess('abcd'));
    rter.navigate = jest.fn();

    fixture.detectChanges();

    fillForm(fixture);

    fixture.detectChanges();

    expect(submitButton.nativeElement.getAttribute('disabled')).toEqual(null);
    submitButton.nativeElement.click();

    fixture.detectChanges();

    expect(store.dispatch).toBeCalledTimes(1);
    expect(rter.navigate).toHaveBeenCalledWith(['/cars']);
    expect(sbs.success).toHaveBeenCalled();
  });

  test('should call snackbar.error and never navigate on fail',() => {
    const store = TestBed.get(Store);
    const rter = TestBed.get(Router);
    const sbs = TestBed.get(SnackBarService);
    const submitButton = fixture.debugElement.query(By.css('button[data-testId="btnSubmit"]'));
    actionMock$ = of(addFailed(false));
    rter.navigate = jest.fn();

    fixture.detectChanges();

    fillForm(fixture);

    fixture.detectChanges();

    expect(submitButton.nativeElement.getAttribute('disabled')).toEqual(null);
    submitButton.nativeElement.click();

    fixture.detectChanges();

    expect(store.dispatch).toBeCalledTimes(1);
    expect(rter.navigate).not.toHaveBeenCalled();
    expect(sbs.error).toHaveBeenCalled();
  });

  function fillForm(fture: ComponentFixture<CarFormComponent>) {
    const make = fture.debugElement.query(By.css('input[data-testId="make"]'));
    make.nativeElement.value = 'hon';
    make.nativeElement.dispatchEvent(new Event('input'));

    const model = fture.debugElement.query(By.css('input[data-testId="model"]'));
    model.nativeElement.value = 'hon';
    model.nativeElement.dispatchEvent(new Event('input'));

    const engine = fture.debugElement.query(By.css('input[data-testId="engine"]'));
    engine.nativeElement.value = 'hon';
    engine.nativeElement.dispatchEvent(new Event('input'));

    const bodyType = fture.debugElement.query(By.css('input[data-testId="bodyType"]'));
    bodyType.nativeElement.value = 'hon';
    bodyType.nativeElement.dispatchEvent(new Event('input'));

    const wheel = fture.debugElement.query(By.css('input[data-testId="wheel"]'));
    wheel.nativeElement.value = 4;
    wheel.nativeElement.dispatchEvent(new Event('input'));

    const door = fture.debugElement.query(By.css('input[data-testId="door"]'));
    door.nativeElement.value = 5;
    door.nativeElement.dispatchEvent(new Event('input'));
  }
});
