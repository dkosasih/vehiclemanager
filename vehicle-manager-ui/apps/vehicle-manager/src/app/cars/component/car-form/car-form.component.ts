import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { numberOnlyMask, SnackBarService } from '@vehicle-manager-ui/common';
import { Location } from '@angular/common';
import { Store } from '@ngrx/store';
import { State } from '../../../store/state';
import { Actions, ofType } from '@ngrx/effects';
import { addSuccess, add, addFailed } from '../../../store/action/car.action';
import { Router } from '@angular/router';
import { tap, take } from 'rxjs/operators';

@Component({
  templateUrl: './car-form.component.html',
  styleUrls: ['./car-form.component.scss']
})
export class CarFormComponent implements OnInit {
  carForm: FormGroup;
  formBusy = false;

  constructor(
    private fb: FormBuilder,
    private location: Location,
    private router: Router,
    private store: Store<State>,
    private actions: Actions,
    private snackbarService: SnackBarService
  ) {}

  ngOnInit() {
    this.carForm = this.fb.group({
      id: null,
      vehicleType: 1, // because this is a car form vehicleType always be 1
      make: [null, [Validators.required]],
      model: [null, [Validators.required]],
      engine: [null, [Validators.required]],
      wheel: [null, [Validators.required]],
      door: [null, [Validators.required]],
      bodyType: [null, [Validators.required]]
    });
  }

  returnToUrl() {
    // TODO: do form dirty check and use mat dialog to config cancellation
    // back to the previous url
    this.location.back();
  }

  save() {
    const car = this.carForm.getRawValue();
    this.formBusy = true;
    this.store.dispatch(add(car));

    this.actions.pipe(
      ofType(addSuccess, addFailed),
      tap(x => {
        if (x['id']) {
          this.snackbarService.success("Car added successfully");
          this.router.navigate(['/cars']);
        } else if (!x['is500']) {
          // TODO: should be able to catch server validation error here
          this.snackbarService.error('Something wrong');
        }

        this.formBusy = false;
      }),
      take(1)
    ).subscribe();
  }

  numericMaskConfig(maxDigit: number) {
    return numberOnlyMask(maxDigit);
  }
}
