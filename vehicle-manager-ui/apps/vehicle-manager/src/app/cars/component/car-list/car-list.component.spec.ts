import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CarListComponent } from './car-list.component';
import { MatListModule, MatIconModule } from '@angular/material';
import { By } from '@angular/platform-browser';

describe('CarListComponent', () => {
  let component: CarListComponent;
  let fixture: ComponentFixture<CarListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [CarListComponent],
      imports: [
        MatListModule,
        MatIconModule
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CarListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  test('should render a message stating the list is empty when no car(s) is listed', () => {
    const noCar = fixture.debugElement.query(By.css('[data-testId="noCar"'));

    expect(noCar.nativeElement.innerHTML.trim()).toEqual("No car(s) listed!");
  });

  test('should render a number of car elements when car object array is passed in', () => {
    component.cars = [
      { id: 'a', vehicleType:1, model: 'CRV', make: 'Honda', engine: 'vTec', bodyType: 'SUV', wheel: 4, door: 4 } ,
      { id: 'b', vehicleType:1, model: 'Civic', make: 'Honda', engine: 'vTec', bodyType: 'Sedan', wheel: 4, door: 4 }
    ]

    fixture.detectChanges();

    const carItems = fixture.debugElement.queryAll(By.css('[data-testId="carItem"'));

    expect(carItems.length).toEqual(2);
  });
});
