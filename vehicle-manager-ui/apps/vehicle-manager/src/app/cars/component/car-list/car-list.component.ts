import { Component, OnInit, Input } from '@angular/core';
import { Car } from '../../model/car.model';

@Component({
  selector: 'car-list',
  templateUrl: './car-list.component.html',
  styleUrls: ['./car-list.component.scss']
})
export class CarListComponent implements OnInit {
  @Input()
  cars: Car[];

  constructor() { }

  ngOnInit() {
  }

}
