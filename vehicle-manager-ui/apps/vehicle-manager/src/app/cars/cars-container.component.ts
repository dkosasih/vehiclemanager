import { Component, OnInit } from '@angular/core';
import { Car } from './model/car.model';
import { State } from '../store/state';
import { Store, select } from '@ngrx/store';
import { Observable } from 'rxjs';
import { load } from '../store/action/car.action';
import { getCars } from '../store/selector/car.selector';

@Component({
  templateUrl: './cars-container.component.html',
  styleUrls: ['./cars-container.component.scss']
})
export class CarsContainerComponent implements OnInit {
  cars$: Observable<Car[]> = this.store.pipe(select(getCars));
  constructor(
    private store: Store<State>
  ) { }

  ngOnInit() {
    this.store.dispatch(load());
  }
}
