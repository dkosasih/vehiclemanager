import {
  ActionReducer,
  ActionReducerMap,
  createFeatureSelector,
  createSelector,
  MetaReducer
} from '@ngrx/store';
import { storeFreeze } from 'ngrx-store-freeze';
import { environment } from '../../../environments/environment';
import { State } from '../state';
import { carReducers } from '../reducers/car.reducer';


export const reducers: ActionReducerMap<State> = {
  cars: carReducers
};

export const metaReducers: MetaReducer<State>[] = !environment.production ? [storeFreeze] : [];
