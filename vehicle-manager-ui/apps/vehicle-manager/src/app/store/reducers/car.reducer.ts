import { initialCarState } from '../state/car.state';
import { loadSuccess, loadFailed, searchFailed, searchSuccess } from '../action/car.action';
import { createReducer, on } from '@ngrx/store';

export const carReducers = createReducer(
  initialCarState,
  on(
    loadSuccess,
    loadFailed,
    (state, action) => ({ ...state, cars: action.payload })
  ),
  on(
    searchSuccess,
    searchFailed,
    (state, action) => ({ ...state, selectedCar: action.payload })
  )
);
