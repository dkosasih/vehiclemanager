import {createAction, union} from '@ngrx/store';
import { Car } from '../../cars/model/car.model';

export const load = createAction(
  '[car] load'
);
export const loadSuccess = createAction(
  '[car] load success',
  (payload: Car[]) => ({ payload })
);
export const loadFailed = createAction(
  '[car] load failed',
  (payload: Car[], is500: boolean) => ({ payload, is500 })
);
export const add = createAction(
  '[car] add',
  (payload: Car) => ({ payload })
);
export const addSuccess = createAction(
  '[car] add success',
  (id: string) => ({ id })
);
export const addFailed = createAction(
  '[car] add failed',
  (is500: boolean) => ({ is500 })
);
export const search = createAction(
  '[car] search',
  (id: string) => ({ id })
);
export const searchSuccess = createAction(
  '[car] search success',
  (payload: Car) => ({ payload })
);
export const searchFailed = createAction(
  '[car] searchFailed',
  (payload: Car, is500: boolean) => ({ payload, is500 })
);
