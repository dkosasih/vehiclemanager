import { createFeatureSelector, createSelector } from '@ngrx/store';
import { State } from '../state';
import { CarState } from '../state/car.state';

export const carFeature = createFeatureSelector<State, CarState>('cars');

export const getCars = createSelector(carFeature, (c => c.cars));
export const getSelectedCar = createSelector(carFeature, (c => c.selectedCar));
