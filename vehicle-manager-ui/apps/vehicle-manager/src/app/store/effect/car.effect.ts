import { Injectable } from '@angular/core';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { load, loadSuccess, loadFailed, add, addSuccess, addFailed } from '../action/car.action';
import { switchMap, catchError, map, exhaustMap } from 'rxjs/operators';
import { CarService } from '../../cars/service/car.service';
import { of } from 'rxjs';

@Injectable({providedIn:'root'})
export class CarEffects {

  constructor(
    private actions$: Actions,
    private carData: CarService
  ) { }

  @Effect()
  loadCars$ = this.actions$.pipe(
    ofType(load),
    switchMap(car => this.carData.getCars().pipe(
      // TODO: add commandResult complex type to defined if error is server validation error or exceptions (500)
      map(res => loadSuccess(res)),
      catchError(err => of(loadFailed([], err.is500)))
    ))
  );

  @Effect()
  addCars$ = this.actions$.pipe(
    ofType(add),
    exhaustMap(car => this.carData.addCar(car.payload).pipe(
      map(res => addSuccess(res.id)),
      catchError(err => of(addFailed(err.is500)))
    ))
  );
}
