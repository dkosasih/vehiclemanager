import { CarState, initialCarState } from './car.state';

export interface State {
  cars: CarState;
}

export const initialState: State = {
  cars: initialCarState
}

export function getInitialState(): State {
  return initialState;
}
