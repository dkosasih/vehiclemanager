import { Car } from '../../cars/model/car.model';

export interface CarState {
  cars: Car[];
  selectedCar: Car;
}

export const initialCarState: CarState = {
  cars: null,
  selectedCar: null
};
