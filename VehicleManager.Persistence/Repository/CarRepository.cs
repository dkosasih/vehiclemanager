﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VehicleManager.Domain.Entity.CarEntity;

namespace VehicleManager.Persistence.Repository
{
    /// <summary>
    /// Repository implementation for a specific type of vehicle (Car)
    /// </summary>
    public class CarRepository : ICarRepository
    {
        private readonly VehicleDbContext _vehicleDbContext;
        public CarRepository(VehicleDbContext dbContext)
        {
            _vehicleDbContext = dbContext;
        }

        public async Task<IList<Car>> GetAll()
        {
            return  await _vehicleDbContext.Vehicles.OfType<Car>().AsNoTracking().ToListAsync();
        }

        public async Task<Car> GetById(string id)
        {
            return await _vehicleDbContext.Vehicles.OfType<Car>().AsNoTracking().SingleOrDefaultAsync(r => r.Id == id);
        }

        public async Task<string> Add(Car car)
        {
            if (car != null)
            {
                _vehicleDbContext.Vehicles.Add(car);
                await _vehicleDbContext.SaveChangesAsync();

                return car.Id;
            }

            return null;
        }

        public async Task Delete(string id)
        {
            var car = await _vehicleDbContext.Vehicles.FindAsync(id);
            if (car != null)
            {
                _vehicleDbContext.Vehicles.Remove(car);
                await _vehicleDbContext.SaveChangesAsync();
            }
        }

        public async Task Update(string id, Car car)
        {
            var currentCar = await _vehicleDbContext.Vehicles.OfType<Car>().SingleOrDefaultAsync(r => r.Id == id);
            if (currentCar != null)
            {
                currentCar.Make = car.Make;
                currentCar.Model = car.Model;
                currentCar.Engine = car.Engine;
                currentCar.BodyType = car.BodyType;
                currentCar.Door = car.Door;
                currentCar.Wheel = car.Wheel;

                await _vehicleDbContext.SaveChangesAsync();
            }
        }
    }
}
