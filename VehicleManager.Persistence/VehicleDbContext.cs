﻿using Microsoft.EntityFrameworkCore;
using System;
using VehicleManager.Domain.Base;
using VehicleManager.Domain.Entity.CarEntity;
using VehicleManager.Persistence.EntityConfiguration;

namespace VehicleManager.Persistence
{
    /// <summary>
    /// Vehicle DbContext that is potentially hold more than one type of vehicle
    /// </summary>
    public class VehicleDbContext : DbContext
    {
        public DbSet<Vehicle> Vehicles { get; set; }

        public VehicleDbContext(DbContextOptions<VehicleDbContext> options)
            : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.Entity<Car>().ToTable("Car");

            builder.Entity<Vehicle>().HasKey(p => p.Id);
            builder.Entity<Vehicle>().Property(p=>p.Id).ValueGeneratedOnAdd();

            builder.Entity<Vehicle>().Property<string>("Discriminator").HasMaxLength(200);

            builder.ApplyConfiguration(new CarConfiguration());
        }
    }
}
