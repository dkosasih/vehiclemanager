﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;
using VehicleManager.Domain.Base;
using VehicleManager.Domain.Entity.CarEntity;

namespace VehicleManager.Persistence.EntityConfiguration
{
    /// <summary>
    /// Configuration class to define how the object is mapped in the database
    /// </summary>
    public class CarConfiguration : IEntityTypeConfiguration<Car>
    {
        public void Configure(EntityTypeBuilder<Car> builder)
        {
            builder
                .Property(p => p.Wheel)
                .HasColumnName("Wheels (Number of)");
            builder
                .Property(p => p.Door)
                .HasColumnName("Doors (Number of)");

            builder.HasBaseType<Vehicle>();
        }
    }
}
