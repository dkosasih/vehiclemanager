﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using VehicleManager.Application.Base;
using VehicleManager.Application.Command;
using VehicleManager.Application.Dto;
using VehicleManager.Application.Query;

namespace VehicleManager.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CarController : ControllerBase
    {
        private readonly IMediator _mediator;
        public CarController(IMediator mediator)
        {
            _mediator = mediator;
        }

        [HttpGet]
        public async Task<IList<CarDto>> Get()
        {
            return await _mediator.Send(new CarsQuery());
        }

        [HttpGet("{id}")]
        public async Task<CarDto> Get(string id)
        {
            return await _mediator.Send(new CarQuery(id));
        }

        // POST: api/Car
        [HttpPost]
        public async Task<CommandResult> Post([FromBody] CarDto carDto)
        {
            return await _mediator.Send(new CarCommand(carDto));
        }

        // PUT: api/Car/5
        [HttpPut("{id}")]
        public async Task<CommandResult> Put(int id, [FromBody] CarDto carDto)
        {
            return await _mediator.Send(new CarCommand(carDto.Id, carDto));
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public async Task<CommandResult> Delete(string id)
        {
            return await _mediator.Send(new CarCommand(id));
        }
    }
}
