﻿using Autofac;
using VehicleManager.Domain.Entity.CarEntity;
using VehicleManager.Persistence.Repository;

namespace VehicleManager.Api.Module
{
    public class RepositoryModule : Autofac.Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<CarRepository>().As<ICarRepository>().InstancePerDependency();
        }
    }
}
