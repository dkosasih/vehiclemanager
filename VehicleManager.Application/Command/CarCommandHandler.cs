﻿using System;
using System.Threading.Tasks;
using VehicleManager.Application.Base;
using VehicleManager.Application.Dto;
using VehicleManager.Application.Enum;
using VehicleManager.Domain.Entity.CarEntity;

namespace VehicleManager.Application.Command
{
    public class CarCommandHandler : BaseCommandHandler<CarCommand>
    {
        private readonly ICarRepository _carRepository;

        public CarCommandHandler(ICarRepository carRepository)
        {
            _carRepository = carRepository;
        }

        protected override Task Validate(ValidationContext context)
        {
            switch (Command)
            {
                case var command when (command.OperationType == OperationType.Add || command.OperationType == OperationType.Update) && command.CarDto == null:
                    context.AddError(nameof(Command.CarDto), "Car cannot be null on Add or Update");
                    break;
                case var command when (command.OperationType == OperationType.Delete || command.OperationType == OperationType.Update)
                && command.Id == null:
                    context.AddError(nameof(Command.CarDto.Id), "Delete or Update operation requires Id");
                    break;
                case var command when command.OperationType != OperationType.Delete && command.CarDto.Door < 3:
                    context.AddError(nameof(Command.CarDto), "Minimum doors for car is 3");
                    break;
                case var command when command.OperationType != OperationType.Delete && command.CarDto.Wheel < 4:
                    context.AddError(nameof(Command.CarDto), "Minimum wheels for car is 4");
                    break;
            };

            return Task.CompletedTask;
        }

        protected override async Task<CommandResult> DoHandle()
        {
            switch (Command.OperationType)
            {
                case OperationType.Add:
                    var id = await _carRepository.Add(ToEntity(Command.CarDto));
                    return new CommandResult(id);
                case OperationType.Delete:
                    await _carRepository.Delete(Command.Id);
                    break;
                case OperationType.Update:
                    await _carRepository.Update(Command.Id, ToEntity(Command.CarDto));
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }

            return new CommandResult();
        }

       private Car ToEntity(CarDto carDto)
        {
            return new Car
            {
                VehicleType = carDto.VehicleType,
                Make = carDto.Make,
                Model = carDto.Model,
                Engine = carDto.Engine,

                Wheel = carDto.Wheel,
                Door = carDto.Door,
                BodyType = carDto.BodyType
            };
        }
    }
}
