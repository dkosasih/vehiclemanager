﻿using VehicleManager.Application.Dto;
using VehicleManager.Application.Enum;
using VehicleManager.Application.Interface;

namespace VehicleManager.Application.Command
{
    public class CarCommand : ICommand
    {
        public OperationType OperationType { get; set; }
        public string Id { get; set; }

        public CarDto CarDto { get; set; }

        public CarCommand(string id)
        {
            OperationType = OperationType.Delete;
            Id = id;
        }

        public CarCommand(CarDto carDto)
        {
            OperationType = OperationType.Add;
            CarDto = carDto;
        }

        public CarCommand(string id, CarDto carDto)
        {
            OperationType = OperationType.Update;
            CarDto = carDto;
            Id = id;
        }
    }
}
