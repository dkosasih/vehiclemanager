﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace VehicleManager.Application.Base
{
    /// <summary>
    /// Validation context to store validation results on validate step
    /// </summary>
    public sealed class ValidationContext
    {
        public List<ValidationResult> ValidationResults { get; set; }

        public ValidationContext()
        {
            ValidationResults = new List<ValidationResult>();
        }

        public void AddError(string memberName, string errorMessage)
        {
            ValidationResults.Add(new ValidationResult(errorMessage, new[] { memberName }));
        }

        public void AddErrors(List<ValidationResult> validationResults)
        {
            ValidationResults.AddRange(validationResults);
        }
    }

}
