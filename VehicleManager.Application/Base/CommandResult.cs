﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;

namespace VehicleManager.Application.Base
{
    /// <summary>
    /// Command result to determine if there is error(s)
    /// </summary>
    public class CommandResult
    {
        public bool Success { get { return Errors == null || !Errors.Any(); } }
        public string Id { get; private set; }

        public IList<ValidationResult> Errors { get; set; }

        public CommandResult()
        {
            Errors = new List<ValidationResult>();
        }

        public CommandResult(string id)
        {
            Id = id;
            Errors = new List<ValidationResult>();
        }

        public CommandResult(IList<ValidationResult> errors)
        {
            Errors = errors;
        }
    }
}
