﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using VehicleManager.Application.Interface;

namespace VehicleManager.Application.Base
{
    public abstract class BaseCommandHandler<T> : ICommandHandler<T> where T: ICommand
    {
        public T Command {get;set;}
        public async Task<CommandResult> Handle(T command, CancellationToken cancellationToken)
        {
            BeforeHandle(command);

            Command = command;

            CommandResult result; 

            var context = new ValidationContext();
            await Validate(context);

            if (context.ValidationResults.Any())
            {
                return new CommandResult(context.ValidationResults);
            }

            result = await DoHandle(); 

            await AfterHandle(command);

            return result;
        }

        protected virtual Task AfterHandle(T command) { return Task.CompletedTask; }
        protected virtual void BeforeHandle(T command) { }

        protected abstract Task Validate(ValidationContext context);
        protected abstract Task<CommandResult> DoHandle();
    }
}
