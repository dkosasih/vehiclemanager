﻿using MediatR;
using VehicleManager.Application.Base;

namespace VehicleManager.Application.Interface
{
    public interface ICommandHandler<in T> : IRequestHandler<T, CommandResult> where T: ICommand
    {
    }
}
