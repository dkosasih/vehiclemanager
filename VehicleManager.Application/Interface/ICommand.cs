﻿using MediatR;
using VehicleManager.Application.Base;

namespace VehicleManager.Application.Interface
{
    public interface ICommand : IRequest<CommandResult>
    {
    }
}
