﻿using MediatR;

namespace VehicleManager.Application.Interface
{
    public interface IQuery<out T> : IRequest<T>
    {
    }
}
