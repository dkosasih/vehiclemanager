﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace VehicleManager.Application.Interface
{
    public interface IQueryHandler<in T, TR> : IRequestHandler<T, TR> where T : IRequest<TR>
    {
    }
}
