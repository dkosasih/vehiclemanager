﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VehicleManager.Application.Enum
{
    public enum OperationType
    {
        Delete = 1,
        Update = 2,
        Add = 3
    }
}
