﻿using VehicleManager.Application.Dto;
using VehicleManager.Application.Interface;

namespace VehicleManager.Application.Query
{
    public class CarQuery : IQuery<CarDto>
    {
        public string Id { get; private set; }

        public CarQuery(string id)
        {
            Id = id;
        }
    }
}
