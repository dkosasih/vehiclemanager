﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using VehicleManager.Application.Dto;
using VehicleManager.Application.Interface;
using VehicleManager.Domain.Entity.CarEntity;

namespace VehicleManager.Application.Query
{
    public class CarQueryHandler : IQueryHandler<CarQuery, CarDto>, IQueryHandler<CarsQuery, IList<CarDto>>
    {
        private readonly ICarRepository _carRepository;
        public CarQueryHandler(ICarRepository carRepository)
        {
            _carRepository = carRepository;
        }

        public async Task<CarDto> Handle(CarQuery query, CancellationToken cancellationToken)
        {
            return ToDto(await _carRepository.GetById(query.Id));
        }

        public async Task<IList<CarDto>> Handle(CarsQuery query, CancellationToken cancellationToken)
        {
            return ToDto(await _carRepository.GetAll());
        }

        // NOTE: this would be a one to one map at the moment, this will change when lookup value is introduced
        private CarDto ToDto(Car car)
        {
            return new CarDto
            {
                Id = car.Id,

                VehicleType = car.VehicleType,
                Make = car.Make,
                Model = car.Model,
                Engine = car.Engine,

                Wheel = car.Wheel,
                Door = car.Door,
                BodyType = car.BodyType
            };
        }

        private IList<CarDto> ToDto(IList<Car> cars)
        {
            return cars.Select(ToDto).ToList();
        }
    }
}
