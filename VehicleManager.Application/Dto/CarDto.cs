﻿using System;
using System.Collections.Generic;
using System.Text;
using VehicleManager.Domain.Base;

namespace VehicleManager.Application.Dto
{
    public class CarDto : Vehicle
    {
        public int Wheel { get; set; }
        public int Door { get; set; }
        public string BodyType { get; set; }
    }
}
