﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;
using System.Threading;
using System.Threading.Tasks;
using VehicleManager.Application.Command;
using VehicleManager.Application.Dto;
using VehicleManager.Domain.Entity.CarEntity;

namespace VehicleManager.Application.Tests.Command
{
    [TestClass]
    public class CarCommandHandlerTests
    {
        private CarCommandHandler _commandHandler;
        private ICarRepository _carRepository;

        [TestInitialize]
        public void Init()
        {
            _carRepository = Substitute.For<ICarRepository>();

            _commandHandler = new CarCommandHandler(_carRepository);
        }

        [TestMethod]
        public async Task DispatchCommandWithRouteAndIdParameter_RepositoryUpdateMethodCalledWithTransformedValue()
        {
            // Arrange
            var carDto = new CarDto
            {
                Id = "car001",
                Make = "Honda",
                BodyType = "Sedan",
                VehicleType = 1,
                Door = 4,
                Engine = "VTech",
                Model = "Accord",
                Wheel = 4
            };

            // Act
            var result = await _commandHandler.Handle(new CarCommand(carDto.Id, carDto), new CancellationToken());

            // Assert 
            await _carRepository.Received().Update(carDto.Id, Arg.Is<Car>(m =>
                m.Make == "Honda" &&
                m.BodyType == "Sedan" &&
                m.Engine == "VTech"
            ));
            Assert.AreEqual(result.Success, true);
        }

        [TestMethod]
        public async Task DispatchCommand_CarObject_RepositoryAddMethodCalledWithTransformedValue()
        {
            // Arrange
            var carDto = new CarDto
            {
                Id = "car001",
                Make = "Honda",
                BodyType = "Sedan",
                VehicleType = 1,
                Door = 4,
                Engine = "VTech",
                Model = "Accord",
                Wheel = 4
            };

            // Act
            var result = await _commandHandler.Handle(new CarCommand(carDto), new CancellationToken());

            // Assert 
            await _carRepository.Received().Add(Arg.Is<Car>(m =>
                m.Make == "Honda" &&
                m.BodyType == "Sedan" &&
                m.Wheel == 4
            ));
            Assert.AreEqual(result.Success, true);
        }

        [TestMethod]
        public async Task DispatchCommand_Id_RepositoryDeleteMethodCalled()
        {
            // Act
            var result = await _commandHandler.Handle(new CarCommand("1"), new CancellationToken());

            // Assert 
            await _carRepository.Received().Delete("1");
            Assert.AreEqual(result.Success, true);
        }

        [TestMethod]
        public async Task DispatchCommand_IdAndCarNull_ValidationFailedWithMessage()
        {
            // Arrange Act
            var result = await _commandHandler.Handle(new CarCommand("1", null), new CancellationToken());

            // Assert 
            Assert.AreEqual(result.Success, false);
            Assert.AreEqual(result.Errors.Count, 1);
            Assert.AreEqual(result.Errors[0].ErrorMessage, "Car cannot be null on Add or Update");
        }

        [TestMethod]
        public async Task DispatchCommand_CarNull_ValidationFailedWithMessage()
        {
            // Arrange
            CarDto car = null;

            // Act
            var result = await _commandHandler.Handle(new CarCommand(car), new CancellationToken());

            // Assert 
            Assert.AreEqual(result.Success, false);
            Assert.AreEqual(result.Errors.Count, 1);
            Assert.AreEqual(result.Errors[0].ErrorMessage, "Car cannot be null on Add or Update");
        }

        [TestMethod]
        public async Task DispatchCommand_IdNull_ValidationFailedWithMessage()
        {
            // Arrange Act
            var result = await _commandHandler.Handle(new CarCommand((string)null), new CancellationToken());

            // Assert 
            Assert.AreEqual(result.Success, false);
            Assert.AreEqual(result.Errors.Count, 1);
            Assert.AreEqual(result.Errors[0].ErrorMessage, "Delete or Update operation requires Id");
        }

        [TestMethod]
        public async Task DispatchCommand_IdNullAndCar_ValidationFailedWithMessage()
        {
            var carDto = new CarDto
            {
                Id = "car001",
                Make = "Honda",
                BodyType = "Sedan",
                VehicleType = 1,
                Door = 4,
                Engine = "VTech",
                Model = "Accord",
                Wheel = 4
            };

            // Arrange Act
            var result = await _commandHandler.Handle(new CarCommand(null, carDto), new CancellationToken());

            // Assert 
            Assert.AreEqual(result.Success, false);
            Assert.AreEqual(result.Errors.Count, 1);
            Assert.AreEqual(result.Errors[0].ErrorMessage, "Delete or Update operation requires Id");
        }

        [TestMethod]
        public async Task DispatchCommand_DoorLessThan3_ValidationFailedWithMessage()
        {
            var carDto = new CarDto
            {
                Id = "car001",
                Make = "Honda",
                BodyType = "Sedan",
                VehicleType = 1,
                Door = 2,
                Engine = "VTech",
                Model = "Accord",
                Wheel = 4
            };

            // Arrange Act
            var result = await _commandHandler.Handle(new CarCommand("carId", carDto), new CancellationToken());

            // Assert 
            Assert.AreEqual(result.Success, false);
            Assert.AreEqual(result.Errors.Count, 1);
            Assert.AreEqual(result.Errors[0].ErrorMessage, "Minimum doors for car is 3");
        }

        [TestMethod]
        public async Task DispatchCommand_WheelLessThan4_ValidationFailedWithMessage()
        {
            var carDto = new CarDto
            {
                Id = "car001",
                Make = "Honda",
                BodyType = "Sedan",
                VehicleType = 1,
                Door = 4,
                Engine = "VTech",
                Model = "Accord",
                Wheel = 3
            };

            // Arrange Act
            var result = await _commandHandler.Handle(new CarCommand("carId", carDto), new CancellationToken());

            // Assert 
            Assert.AreEqual(result.Success, false);
            Assert.AreEqual(result.Errors.Count, 1);
            Assert.AreEqual(result.Errors[0].ErrorMessage, "Minimum wheels for car is 4");
        }
    }
}
