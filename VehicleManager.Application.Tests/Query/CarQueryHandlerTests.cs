﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using VehicleManager.Application.Dto;
using VehicleManager.Application.Query;
using VehicleManager.Domain.Entity.CarEntity;

namespace VehicleManager.Application.Tests.Query
{
    [TestClass]
    public class CarQueryHandlerTests
    {
        private CarQueryHandler _carQueryHandler;
        private ICarRepository _carRepository;

        [TestInitialize]
        public void Init()
        {
            _carRepository = Substitute.For<ICarRepository>();
            _carRepository.GetAll().Returns(
                new List<Car> {
                    new Car { Id = "car001", Make = "Honda", BodyType = "Sedan", VehicleType = 1, Door = 4, Engine = "VTech", Model = "Accord", Wheel = 4 },
                    new Car { Id = "car002", Make = "Toyota", BodyType = "SUV", VehicleType = 1, Door = 4, Engine = "VTech", Model = "Rav4", Wheel = 4 },
                }
            );

            _carRepository.GetById(Arg.Any<string>())
                .Returns(
                    new Car { Id = "car001", Make = "Honda", BodyType = "Sedan", VehicleType = 1, Door = 4, Engine = "VTech", Model = "Accord", Wheel = 4 }
            );

            _carQueryHandler = new CarQueryHandler(_carRepository);
        }

        [TestMethod]
        public async Task DispatchCarsQuery_ListOfCarDtos()
        {
            // Act
            var result = await _carQueryHandler.Handle(new CarsQuery(), new CancellationToken());

            // Assert 
            Assert.AreEqual(result.Count, 2);
            Assert.AreEqual(result.First().Id, "car001");
            Assert.IsTrue(result.First() is CarDto);
        }

        [TestMethod]
        public async Task DispatchCarQuery_Id_SpecificCarDto()
        {
            // Arrange
            var carId = "anyId";

            // Act
            var result = await _carQueryHandler.Handle(new CarQuery(carId), new CancellationToken());

            // Assert 
            Assert.IsTrue(result is CarDto);
            Assert.AreEqual(result.Id, "car001");
        }

    }
}
